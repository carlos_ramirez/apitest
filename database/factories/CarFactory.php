<?php

namespace Database\Factories;

use App\Models\Car;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'marca' => $this->faker->name(),
            'modelo' => $this->faker->lastName(),
            'ano' => '2018',
            'color' => $this->faker->colorName(),
            'capacidadPasajeros' => '5',
            'placa' => Str::random(6),
            'numeroVin' => Str::random(10),
            'numeroMotor' => Str::random(10),
            'activo' => $this->faker->boolean(),
        ];
    }
}
