<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cars', 'App\Http\Controllers\CarController@index');

Route::put('/cars/update/{id}', 'App\Http\Controllers\CarController@update');

Route::post('/cars/guardar', 'App\Http\Controllers\CarController@store');

Route::delete('/cars/borrar/{id}', 'App\Http\Controllers\CarController@destroy');

Route::get('/cars/buscar/{id}', 'App\Http\Controllers\CarController@show');
