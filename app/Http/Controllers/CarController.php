<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;


class CarController extends Controller
{
    public function index(Request $request)
    {
        // listar
        $data = Car::all();
        return $data;

    }

    public function store(Request $request)
    {
        //insertar
        $data = new Car();
        $data->marca = $request->marca;
        $data->modelo = $request->modelo;
        $data->ano = $request->ano;
        $data->color = $request->color;
        $data->capacidadPasajeros = $request->capacidadPasajeros;
        $data->placa = $request->placa;
        $data->numeroVin = $request->numeroVin;
        $data->numeroMotor = $request->numeroMotor;
        $data->activo = $request->activo;
        $data->save();

    }
    public function show($id)
    {
        //mostrar por id
        $data = Car::findOrFail($id);
        return $data;
  }

    public function update(Request $request, $id)
    {
        // actualizar
        $data = Car::findOrFail($id);
        $data->marca = $request->marca;
        $data->modelo = $request->modelo;
        $data->ano = $request->ano;
        $data->color = $request->color;
        $data->capacidadPasajeros = $request->capacidadPasajeros;
        $data->placa = $request->placa;
        $data->numeroVin = $request->numeroVin;
        $data->numeroMotor = $request->numeroMotor;
        $data->activo = $request->activo;
        $data->save();
        return $data;

    }

    public function destroy(Request $request)
    {
        // eliminar
        $data = Car::destroy($request->id);
        return $data;
      }
}
